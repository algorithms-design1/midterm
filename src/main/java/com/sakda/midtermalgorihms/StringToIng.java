/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sakda.midtermalgorihms;

import java.util.Scanner;

/**
 *
 * @author PC Sakda
 */
public class StringToIng {

    public static void main(String[] args) {
        String input = (new Scanner(System.in).next()); // รับค่า
        System.out.println(stringToIng(input));// ปริ้น และ เรียกใช้ method
    }

    private static int stringToIng(String input) {
        int sum = 0; // ผลรวม
        int j = 1; // ตำแหน่งหลัก หรือ ตำแหน่งเลข หลักหน่วย,สิบ,ร้อย,พัน
        for (int i = input.length() - 1; i >= 0; i--) { // ลูปเริ่มต้นจากตำแหน่งสุดท้ายก็คือ หลักหน่วย หรือจากขวามาซ้าย
            if (i == 0 && input.charAt(i) == '-') {// เช็คเงื่อนไข ถ้าเป็นเลข 0 หรือ จำนวนเลขติดลบ ให้ทำการ sum คูณลบหนึ่ง ก็จะได้เลขติดลบ
                sum *= -1;
            } else if (input.charAt(i) == '1') {// เช็คว่าเลข เป็นเลข1ไหม ถ้าใช่ให้ sum += 1 * j (j คือค่าตำแหน่ง ค่าเริ่มต้น=1)
                sum += 1 * j;
            } else if (input.charAt(i) == '2') {// เช็คว่าเลข เป็นเลข2ไหม ถ้าใช่ให้ sum += 2 * j (j คือค่าตำแหน่ง)
                sum += 2 * j;
            } else if (input.charAt(i) == '3') {// เช็คว่าเลข เป็นเลข3ไหม ถ้าใช่ให้ sum += 3 * j (j คือค่าตำแหน่ง)
                sum += 3 * j;
            } else if (input.charAt(i) == '4') {// เช็คว่าเลข เป็นเลข4ไหม ถ้าใช่ให้ sum += 4 * j (j คือค่าตำแหน่ง)
                sum += 4 * j;
            } else if (input.charAt(i) == '5') {// เช็คว่าเลข เป็นเลข5ไหม ถ้าใช่ให้ sum += 5 * j (j คือค่าตำแหน่ง)
                sum += 5 * j;
            } else if (input.charAt(i) == '6') {// เช็คว่าเลข เป็นเลข6ไหม ถ้าใช่ให้ sum += 6 * j (j คือค่าตำแหน่ง)
                sum += 6 * j;
            } else if (input.charAt(i) == '7') {// เช็คว่าเลข เป็นเลข7ไหม ถ้าใช่ให้ sum += 7 * j (j คือค่าตำแหน่ง)
                sum += 7 * j;
            } else if (input.charAt(i) == '8') {// เช็คว่าเลข เป็นเลข8ไหม ถ้าใช่ให้ sum += 8 * j (j คือค่าตำแหน่ง)
                sum += 8 * j;
            } else if (input.charAt(i) == '9') {// เช็คว่าเลข เป็นเลข9ไหม ถ้าใช่ให้ sum += 9 * j (j คือค่าตำแหน่ง)
                sum += 9 * j;
            }
            j = 10 * j; // เป็นการเพิ่มหลัก จากหลักหน่วยขยับมาเป็นหลักสิบ เช่น จาก 21 
            //จากหลักหน่วยก็คือ 1 ตามเงื่อนไขข้างบนจะได้ sum += 1*j(1) จะได้ sum = 1 พอถึงทำบรรทัดนี้ คือ j = 10*1 = 10 
            //พอรอบต่อไปเป็น 2 ก็ทำการ sum(1) += 2*10(ได้จากบรรทัดข้างบน) = 21 ก็คือคำตอบ
        }

        return sum; //ส่งผลลัพธ์กลับไปเป็นชนิด int
    }
}
