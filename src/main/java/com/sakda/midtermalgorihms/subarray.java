package com.sakda.midtermalgorihms;

import java.util.ArrayList;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author PC Sakda
 */
public class subarray {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        ArrayList<Integer> A = new ArrayList<Integer>(); // สร้าง list A ไว้เก็บข้อมูล
        while (kb.hasNext()) {
            A.add(Integer.parseInt(kb.next())); // วนรับข้อมูล และ เพิ่มเข้าไปใน list A
        }
        int n = A.size(); // สร้าง n ไว้สำหรับกำหนดขอบเขต ก็คือ n เท่ากับจำนวนข้อมูลที่อยู่ใน list A ว่ามีกี่ตัว
        System.out.println(subarray(A, n)); // แสดงผล และ เรียกใช้ method subarray
    }

    private static String subarray(ArrayList<Integer> A, int n) { // สร้าง function หรือ method subarray(ส่งlist A และ int n เข้ามา)
        int max = 1, len = 1, maxIndex = 0;  // 'max' เพื่อเก็บความยาวของ subarray ที่เพิ่มขึ้นที่ยาวที่สุด 'len' เพื่อเก็บความยาวของ subarray ที่เพิ่มขึ้นที่ยาวที่สุดในช่วงเวลาที่ต่างกัน
        String result = ""; // result ไว้เก็บ subarray ที่ยาวที่สุดแล้ว return กลับไป
        for (int i = 1; i < n; i++) {
            if (A.get(i) > A.get(i - 1)) { //ถ้าตัวปัจจุบันมากกว่าตัวก่อนหน้า องค์ประกอบนี้จะช่วยในการสร้าง subarray ที่เพิ่มขึ้นก่อนหน้านี้ที่พบจนถึงตอนนี้
                len++;
            } else {
                if (max < len) { //ตรวจสอบว่าความยาว 'max' น้อยกว่าความยาวของ subarray ที่เพิ่มขึ้นในปัจจุบันหรือไม่ 
                    max = len;   // หากเป็นจริง ให้อัปเดต 'max'
                    maxIndex = i - max; // index กำหนดดัชนีเริ่มต้นของ subarray ที่ต่อเนื่องกันที่ยาวที่สุดที่เพิ่มขึ้น
                }
                len = 1;//  รีเซ็ต 'len' เป็น 1
            }
        }
        if (max < len) { // เปรียบเทียบความยาวของ subarray ที่เพิ่มขึ้นล่าสุดกับ 'max'
            max = len;
            maxIndex = n - max;
        }
        for (int i = maxIndex; i < max + maxIndex; i++) { //ลูปหา subarray ที่ยาวที่สุด แล้วนำไปเก็บไว้ในตัวแปร result แล้ว return ค่ากลับไป
                result = result+A.get(i)+" "; // result = result+subarray ตัวที่ i 
        }

        return result; // ส่งค่ากลับไปเป็นชนิด String
    }
}
